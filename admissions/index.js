const db = require('../db')

const createAdmission = async (req, res) => {

    const { date, process: processId } = req.body

    if(!date || !processId)
        return res.status(400).json({ message: "Missing required fields" })

    try {
        
        await db.query('INSERT INTO admission (date, process_id, status_id) VALUES ($1, $2, $3)', [date, processId, 1])
        return res.status(200).json({ message: "Admission inserted successfully" })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }

}

const deleteAdmission = async (req, res) => {

    const admissionId = req.query.id

    if(!admissionId)
        return res.status(400).json({ message: "Missing required fields" })

    try {
        
        await db.query('UPDATE admission SET status_id = 2 WHERE id = $1', [admissionId])
        return res.status(200).json({ message: "Admission deleted successfully" })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }

}

module.exports = {
    createAdmission: createAdmission,
    deleteAdmission: deleteAdmission
}