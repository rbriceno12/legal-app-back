const db = require('../db');

const getCourts = async (req, res) => {

    try {
        const courts = await db.query('SELECT id, name FROM courts WHERE status_id = 1')
        return res.status(200).json({
            message: 'Courts retrieved successfully',
            data: courts.rows
        })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }
}


module.exports = {
    getCourts: getCourts
}