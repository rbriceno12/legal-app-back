const express = require('express');

const app        = express()
const auth       = require('./auth/index')
const cases      = require('./cases/index')
const processes  = require('./processes/index')
const courts     = require('./courts/index')
const admissions = require('./admissions/index')
const citations  = require('./citations/index')


const bodyParser = require('body-parser');

require('dotenv').config(); // Load environment variables from .env file

const port = process.env.PORT || 3000; // Use environment variable or default port


// Middleware
app.use(bodyParser.json()); // Parse JSON data in request body
app.use(bodyParser.urlencoded({ extended: true })); // Parse form data


//Authorization/Users
app.post('/register', (req, res) => {

  auth.registerUser(req, res)
  
});

app.post('/login', (req, res) => {

  auth.loginUser(req, res)

});

//CRUD cases
app.get('/cases', auth.verifyToken, (req, res) => {

  cases.getCases(req, res)

})

app.post('/cases', auth.verifyToken, (req, res) => {

  cases.createCases(req, res)

})

app.delete('/cases', auth.verifyToken, (req, res) => {

  cases.deleteCases(req, res)

})


//CRUD processes

app.post('/processes', auth.verifyToken, (req, res) => {

  processes.createProcess(req, res)

})


//CRUD courts

app.get('/courts', auth.verifyToken, (req, res) => {

  courts.getCourts(req, res)

})


//CRUD processes

app.post('/processes', auth.verifyToken, (req, res) => {

  processes.createProcess(req, res)

})

app.delete('/processes', auth.verifyToken, (req, res) => {

  processes.deleteProcess(req, res)

})

//CRUD admissions

app.post('/admissions', auth.verifyToken, (req, res) => {

  admissions.createAdmission(req, res)

})

app.delete('/admissions', auth.verifyToken, (req, res) => {

  admissions.deleteAdmission(req, res)

})

//CRUD citations

app.post('/citations', auth.verifyToken, (req, res) => {

  citations.createCitation(req, res)

})

app.delete('/citations', auth.verifyToken, (req, res) => {

  citations.deleteCitation(req, res)

})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})