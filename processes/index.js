const db = require('../db');


const createProcess = async (req, res) => {

    const {parts: partsSelection, court: courtId, type: processType, case: caseId} = req.body

    if(!partsSelection || !courtId || !processType || !caseId)
        return res.status(400).json({ message: "Missing required fields" })

    try {
        
        await db.query('INSERT INTO processes (parts_selection, court_id, process_type, case_id, status_id) VALUES ($1, $2, $3, $4, $5)', [partsSelection, courtId, processType, caseId, 1])
        return res.status(200).json({ message: "Process created successfully" })

    } catch (error) {
        
        return res.status(500).json({
            message: "Internal server error",
        })

    }

}


const deleteProcess = async (req, res) => {

    const processId = req.query.id

    if(!processId)
        return res.status(400).json({ message: "Process id is required" })

    try {

        await db.query('UPDATE processes SET status_id = 2 WHERE id = $1', [processId])
        return res.status(200).json({ message: "Process deleted successfully" })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }

}


module.exports = {
    createProcess: createProcess,
    deleteProcess: deleteProcess
}