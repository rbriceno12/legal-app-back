const db = require('../db');

const getCases = async (req, res) => {

    const userId = req.userId
  
    try {
        const cases = await db.query('SELECT * FROM cases WHERE user_id = $1 AND status_id = 1', [userId])
        return res.status(200).json({
            message: "Cases retrieved successfully",
            data: cases.rows
        })
    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }
}

const createCases = async (req, res) => {

    const userId = req.userId
    const { name } = req.body

    if(!name)
        return res.status(400).json({ message: "The name of the case is required" })

    try {
        
        const cases = await db.query('INSERT INTO cases (name, user_id, status_id) VALUES ($1, $2, $3)', [name, userId, 1])
        return res.status(200).json({
            message: "Case inserted successfully",
        })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }
}

const deleteCases = async (req, res) => {

    const id = req.query.id

    if(!id)
        return res.status(400).json({ message: "The id of the case is required" })

    try {
        
        const cases = await db.query('UPDATE cases SET status_id = 2 WHERE id = $1', [id])
        return res.status(200).json({
            message: "Case deleted successfully",
        })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }
}

module.exports = {
    getCases: getCases,
    createCases: createCases,
    deleteCases: deleteCases
}