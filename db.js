const { Pool } = require('pg');


//AWS server
// const pool = new Pool({
//   user: 'postgres',
//   password: 'postgres',
//   host: '54.203.48.99',
//   port: 5432, // default Postgres port
//   database: 'postgres'
// });


//Localhost
const pool = new Pool({
  user: 'postgres',
  password: 'postgres',
  host: 'localhost',
  port: 5432, // default Postgres port
  database: 'legal_app'
});

module.exports = {
  query: (text, params) => pool.query(text, params)
};