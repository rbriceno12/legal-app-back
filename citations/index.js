const db = require('../db')

const createCitation = async (req, res) => {

    const { expiration: expirationDate, admission: admissionId } = req.body

    if(!expirationDate)
        return res.status(400).json({ message: "Missing required fields" })

    try {
        
        await db.query('INSERT INTO citations (expiration_date, admission_id, status_id) VALUES ($1, $2, $3)', [expirationDate, admissionId, 1])
        return res.status(200).json({ message: "Citation created successfully" })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }

}

const deleteCitation = async (req, res) => {

    const citationId = req.query.id

    if(!citationId)
        return res.status(400).json({ message: "Missing required fields" })

    try {
        
        await db.query('UPDATE citations SET status_id = 2 WHERE id = $1', [citationId])
        return res.status(200).json({ message: "Citation deleted successfully" })

    } catch (error) {

        return res.status(500).json({
            message: "Internal server error",
        })

    }

}

module.exports = {
    createCitation: createCitation,
    deleteCitation: deleteCitation
}